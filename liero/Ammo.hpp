//
//  Ammo.hpp
//  liero
//
//  Created by Marko Lehtola on 20/02/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#ifndef Ammo_hpp
#define Ammo_hpp

#include <stdio.h>
#include "GameObject.hpp"

class Ammo : public GameObject{
    
public:
    
    void setAimCoords(double x, double y){
        this->aim_x = x;
        this->aim_y = y;
    };
    
    void fire();
    
    void update();
    
private:
    
    double aim_x;
    double aim_y;
    
    //is the ammo live or not?
    bool bLive = false;
    
};
#endif /* Ammo_hpp */
