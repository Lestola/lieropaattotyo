//
//  Level.hpp
//  Liero
//
//  Created by Teemu Saarelainen on 15/01/2019.
//  Copyright © 2019 Teemu Saarelainen. All rights reserved.
//

#ifndef Level_hpp
#define Level_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>

class Level {
    
public:
    
    Level();
    void randomize();
    
    sf::Image leveldata;
    
    const int width = 1200;
    const int height = 800;
    const int nBlocks = 35;
    const int minBlockW = this->width / 20;
    const int minBlockH = this->height / 20;
    
    const int minRadius = this->height / 20;
    const int maxRadius = this->height / 8;
    
    void makeSafeBubble(int x, int y);
    
private:
    
    void generateBlock();
    void generateCircle();
    
    
};

#endif /* Level_hpp */
