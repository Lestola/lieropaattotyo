//
//  Game.cpp
//  Liero
//
//  Created by Teemu Saarelainen on 15/01/2019.
//  Copyright © 2019 Teemu Saarelainen. All rights reserved.
//

#include "Game.hpp"
#include <iostream>

Game::Game(){
    // Randomize the level when a game is created
    this->level.randomize();
    
    // Set the position of the player
    this->player.reset(this->level.width / 2,
                       this->level.height / 2);
    
    this->level.makeSafeBubble(player.getX(), player.getY());
    
    this->ammo.reset(this->level.width / 2,
                     this->level.height / 2);
}

void Game::update(){
    // What needs to be done during each frame?
    // E.g. update each game object
    //std::cout << "Update called!" << std::endl;
    //this->level.randomize();
    
    this->player.update();
    this->ammo.update();
    
    //maa kosketus havaittu
    if(this->checkCollisionBottom()) {
        this->player.setYSpeed(-0.01f);
    }
    if(this->checkCollisionAbove()) {
        this->player.setYSpeed(this->player.getYSpeed() * -1.0f);
    }
    if(this->checkCollisionLeft()){
        this->player.setXSpeed(this->player.getXSpeed() * -1.0f);
    }
    if(this->checkCollisionRight()){
        this->player.setXSpeed(this->player.getXSpeed() * -1.0f);
    }
    
}

// This function returns the current data for the level
sf::Image Game::getLevelData(){
    return this->level.leveldata;
}

bool Game::checkCollisionBottom(){
    
    //käy kaikki pikselit läpi, jotka ovat pelaajan alapuolella
    int x = this->player.getX()-player.width/2;
    int y = this->player.getY()+player.height/2+1;
    
    for (int i = x; i < x+this->player.width; i++){
        if(this->level.leveldata.getPixel(i, y) == sf::Color::Black){
            return true;
        }
    }
    //ei löytynyt mustaa
    return false;
}

bool Game::checkCollisionAbove(){
    
    //käy kaikki pikselit läpi, jotka ovat pelaajan alapuolella
    int x = this->player.getX()-player.width/2;
    int y = this->player.getY()-player.height/2-1;
    
    for (int i = x; i < x+this->player.width; i++){
        if(this->level.leveldata.getPixel(i, y) == sf::Color::Black){
            return true;
        }
    }
    //ei löytynyt mustaa
    return false;
}

bool Game::checkCollisionLeft(){
    
    //käy kaikki pikselit läpi, jotka ovat pelaajan alapuolella
    int x = this->player.getX() - player.width/2 + 1;
    int y = this->player.getY() - player.height/2;
    
    for (int j = y; j < y + this->player.height; j++){
        if(this->level.leveldata.getPixel(x, j) == sf::Color::Black){
            return true;
        }
    }
    //ei löytynyt mustaa
    return false;
}

bool Game::checkCollisionRight(){
    
    //käy kaikki pikselit läpi, jotka ovat pelaajan alapuolella
    int x = this->player.getX() + player.width/2 + 1;
    int y = this->player.getY() - player.height/2;
    
    for (int j = y; j < y + this->player.height; j++){
        if(this->level.leveldata.getPixel(x, j) == sf::Color::Black){
            return true;
        }
    }
    //ei löytynyt mustaa
    return false;
}
