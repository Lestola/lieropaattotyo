//
//  InputHandler.hpp
//  Liero
//
//  Created by Teemu Saarelainen on 22/01/2019.
//  Copyright © 2019 Teemu Saarelainen. All rights reserved.
//
#ifndef InputHandler_hpp
#define InputHandler_hpp

#include <stdio.h>
#include "Game.hpp"
#include <SFML/Graphics.hpp>

class InputHandler {
    
public:
    InputHandler(sf::RenderWindow* w, Game* pG);
    void processEvents();
    
private:
    
    sf::RenderWindow* pWindow;
    //Player* pPlayer;
    Game* pGame;
    
    // Try to do a boolean system for controls
    bool bPlayerMoveUp = false;
    bool bPlayerMoveDown = false;
    bool bPlayerMoveLeft = false;
    bool bPlayerMoveRight = false;
    
    bool bPlayerShoot = false;
    bool bGraplingHookSwing = false;
    
    // What about player two???
    //bool bPlayerTwoMoveUp = false;
};

#endif /* InputHandler_hpp */
