//
//  Player.cpp
//  Liero
//
//  Created by Teemu Saarelainen on 22/01/2019.
//  Copyright © 2019 Teemu Saarelainen. All rights reserved.
//

#include "Player.hpp"

Player::Player(){
    
}

void Player::update(){
    
    GameObject::update();
    
    this->x += this->v_x;
    this->y += this->v_y;
    
    // Gravity effect
//    if (gravityOn){
//    this->v_y += 0.001;
//    }
    
    // Horizontal speed check:
    if (this->v_x > 0.5f)
        this->v_x = 0.5f;
    
    if (this->v_x < -0.5f)
        this->v_x = -0.5f;
    
    // Limit checks: TODO: MOVE THESE INTO GAME-CLASS!?!?!
    // Lower limit
    if (this->y + this->height/2 > 800.0f){
        this->y = 800.0f - this->height/2;
        this->v_y = 0.0f;
    }
    // Upper limit
    if (this->y - this->height/2 < 0.0f){
        this->y = (float)(+this->height/2);
        this->v_y = 0.0f;
    }
    
    // Left limit
    if (this->x - this->width/2 < 0.0f){
        this->x = (float)(+this->width/2);
        this->v_x = 0.0f;
    }
    // Right limit
    if (this->x + this->width/2 > 1200.0f){
        this->x = (float)(1200.0f - this->width/2);
        this->v_x = 0.0f;
    }
};
