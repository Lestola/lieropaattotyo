//
//  InputHandler.cpp
//  Liero
//
//  Created by Teemu Saarelainen on 22/01/2019.
//  Copyright © 2019 Teemu Saarelainen. All rights reserved.
//

#include "InputHandler.hpp"
#include <iostream>

InputHandler::InputHandler(sf::RenderWindow* w, Game* pG){
    this->pWindow = w;
    this->pGame = pG; // This is the player that will be moved etc.
    
    //for (int i=0;i<4;i++)
    //   this->aControls[i] = false;
}

void InputHandler::processEvents(){
    
    sf::Event event;
    while (this->pWindow->pollEvent(event)){
        
        // Close window: exit
        if (event.type == sf::Event::Closed) {
            pWindow->close();
        }
        
        // Escape pressed: exit
        if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
            pWindow->close();
        }
        
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            // left mouse button is pressed: shoot
            //this->bPlayerShoot = true;
            pGame->ammo.setAimCoords(event.mouseButton.x, event.mouseButton.y);
            pGame->ammo.fire();
            std::cout << event.mouseButton.x << "," << event.mouseButton.y << std::endl;
            
        } else {
            //left mouse button is NOT pressed: stop shooting
            this->bPlayerShoot = false;
        }
        
        if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
        {
            this->bGraplingHookSwing = true;
        } else {
            this->bGraplingHookSwing = false;
        }
        
        if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::G) {
            pGame->player.toggleGravity();
            
            
        }
        
        if (event.type == sf::Event::KeyPressed){
            switch (event.key.code) {
                case sf::Keyboard::W:
                    this->bPlayerMoveUp = true;
                    break;
                case sf::Keyboard::A:
                    this->bPlayerMoveLeft = true;
                    break;
                case sf::Keyboard::S:
                    this->bPlayerMoveDown = true;
                    break;
                case sf::Keyboard::D:
                    this->bPlayerMoveRight = true;
                    break;
                default:
                    break;
            }
        }
        
        if (event.type == sf::Event::KeyReleased){
            switch (event.key.code) {
                case sf::Keyboard::W:
                    this->bPlayerMoveUp = false;
                    break;
                case sf::Keyboard::A:
                    this->bPlayerMoveLeft = false;
                    break;
                case sf::Keyboard::S:
                    this->bPlayerMoveDown = false;
                    break;
                case sf::Keyboard::D:
                    this->bPlayerMoveRight = false;
                    break;
                default:
                    break;
            }
        }
        
        // Do the moves!!! But only if the event came from the keyboard, so the mouse
        // doesn't affect this...
        if (event.type == sf::Event::KeyPressed){
            if (bPlayerMoveUp)
                pGame->player.moveUp();
            if (bPlayerMoveLeft)
                pGame->player.moveLeft();
            if (bPlayerMoveDown)
                pGame->player.moveDown();
            if (bPlayerMoveRight)
                pGame->player.moveRight();
        }
        
        if (event.type == sf::Event::MouseButtonPressed){
            if (bPlayerShoot)
                pGame->player.shoot();
        }
    }
}
