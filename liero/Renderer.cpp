//
//  Renderer.cpp
//  Liero
//
//  Created by Teemu Saarelainen on 15/01/2019.
//  Copyright © 2019 Teemu Saarelainen. All rights reserved.
//

#include "Renderer.hpp"

Renderer::Renderer(sf::RenderWindow* pw, Game* pg){
    
    // Set our render window target
    this->pWindow = pw;
    
    // Set pointer to game object
    this->pGame = pg;
    
    //set ammo radius
    this->circle.setRadius(ammoRadius);
    this->circle.setFillColor(sf::Color::Blue);
    
    this->counter = 0;
}

void Renderer::render(){
    
    counter++;
    if (counter > 10000000)
        counter = 0;
    
    // Clear the data
    pWindow->clear();
    
    //set the rect position to match player
    rect.setPosition(pGame->player.getX()-pGame->player.width/2,
                     pGame->player.getY()-pGame->player.height/2);
    
    //rect.setFillColor(sf::Color(253,124,178));
    rect.setFillColor(sf::Color(226,68,68));
    rect.setSize(sf::Vector2f(pGame->player.width,pGame->player.height));
    
    //set the circhle to match the ammo
    circle.setPosition(pGame->ammo.getX()-(ammoRadius/2), pGame->ammo.getY()-(ammoRadius/2));
    
    //ladataan nykyisen levelin data level objektilta
    texture.loadFromImage(this->pGame->getLevelData());
    sprite.setTexture(texture);
    
    //
    //piirretään tavaraa
    //
    
    //piirretään pelin tausta
    pWindow->draw(sprite);
    
    //pWindow->draw(circle);
    pWindow->draw(rect);
    
    //draw the ammo
    pWindow->draw(circle);
    
    // Push the data to the display
    pWindow->display();
}
