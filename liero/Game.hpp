//
//  Game.hpp
//  Liero
//
//  Created by Teemu Saarelainen on 15/01/2019.
//  Copyright © 2019 Teemu Saarelainen. All rights reserved.
//

#ifndef Game_hpp
#define Game_hpp
#include "Level.hpp"
#include "Player.hpp"
#include "Ammo.hpp"
#include <stdio.h>

class Game {
    
public:
    
    Game();
    
    void update();
    
    sf::Image getLevelData();
    
    Player player;
    
    Ammo ammo;
    
private:
    
    Level level;
    
    bool checkCollisionBottom();
    bool checkCollisionAbove();
    bool checkCollisionLeft();
    bool checkCollisionRight();
    
};

#endif /* Game_hpp */
