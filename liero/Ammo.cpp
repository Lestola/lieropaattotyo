//
//  Ammo.cpp
//  liero
//
//  Created by Marko Lehtola on 20/02/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#include "Ammo.hpp"
#include <cmath>

void Ammo::fire(){
    
    //set the velocity of the ammo correctly
    double xdiff = this->aim_x - this->x;
    double ydiff = this->aim_y - this->y;
    
    double hyp = sqrt(xdiff * xdiff + ydiff * ydiff);
    //the total velocity is 10
    this->v_x = 10.0 * xdiff / hyp;
    this->v_y = 10.0 * ydiff / hyp;
    
    //set this ammo LIVE!!
    this->bLive = true;
};

void Ammo::update(){
    if(this->bLive){
        this->x += v_x;
        this->y += v_y;
    }
};
