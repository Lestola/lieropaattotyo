//
//  Renderer.hpp
//  Liero
//
//  Created by Teemu Saarelainen on 15/01/2019.
//  Copyright © 2019 Teemu Saarelainen. All rights reserved.
//

#ifndef Renderer_hpp
#define Renderer_hpp

#include <stdio.h>

#include "SFML/Graphics.hpp"
#include "Level.hpp"
#include "Game.hpp"

class Renderer {
    
public:
    // Constructor, takes a pointer to renderwindow as a parameter
    Renderer(sf::RenderWindow* pw, Game* pg);
    
    // This method does all the magic!!!
    void render();
    
private:
    // We need a member variable where we render everything
    sf::RenderWindow* pWindow;
    Game* pGame;
    
    int counter;
    int ammoRadius = 10;
    sf::CircleShape circle;
    sf::RectangleShape rect;
    
    sf::Texture texture;
    sf::Sprite sprite;
    
    //Level level;
    
};

#endif /* Renderer_hpp */
