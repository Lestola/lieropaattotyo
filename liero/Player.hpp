//
//  Player.hpp
//  Liero
//
//  Created by Teemu Saarelainen on 22/01/2019.
//  Copyright © 2019 Teemu Saarelainen. All rights reserved.
//

#ifndef Player_hpp
#define Player_hpp

#include <stdio.h>
#include "GameObject.hpp"

class Player : public GameObject {
    
public:
    
    Player();
    void moveLeft(){this->v_x-=.02;};
    void moveRight(){this->v_x+=.02;};
    void moveUp(){this->v_y-=.4;};
    void moveDown(){this->v_y+=.1;};
    void shoot(){this->v_y-=.4;};
    
    
    void update();
    
    const int width = 40;
    const int height = 80;
    
private:
    
};

#endif /* Player_hpp */
